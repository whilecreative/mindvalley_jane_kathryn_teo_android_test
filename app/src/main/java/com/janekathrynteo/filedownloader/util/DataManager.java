package com.janekathrynteo.filedownloader.util;

import android.util.Log;

import com.janekathrynteo.filedownloader.model.Category;
import com.janekathrynteo.filedownloader.model.Link;
import com.janekathrynteo.filedownloader.model.PinData;
import com.janekathrynteo.filedownloader.model.ProfileImage;
import com.janekathrynteo.filedownloader.model.Url;
import com.janekathrynteo.filedownloader.model.User;
import com.janekathrynteo.filedownloader.model.WebAPIData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Janek on 16/10/2016.
 */

public class DataManager implements ApiManager.ApiListener {

    private static final String TAG = "DataManager";
    private DataListener dataListener;

    public void getPinData(WebAPIData source, DataListener listener) {

        try {
            dataListener = listener;
            ApiManager asyncPoster = new ApiManager(this);
            asyncPoster.execute(source);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onRemoteCallComplete(String responseData) {

        Log.d(TAG, "data received");
        try {
            JSONArray arrayObj = new JSONArray(responseData);

            if (arrayObj != null) {
                int dataSize = arrayObj.length();
                int catSize = 0;
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                ArrayList<PinData> pinlist = new ArrayList<PinData>();
                PinData temp;
                User userTemp;
                Link linkTemp;
                Url urlTemp;
                Category categoryTemp;
                ArrayList<Category> catLisTemp;
                ProfileImage profileTemp;
                JSONObject nestedObj;
                JSONObject superNestedObj;
                JSONArray nestedArrayObj;

                for (int i = 0; i < dataSize; i++) {
                    JSONObject single = (JSONObject) arrayObj.get(i);
                    temp = new PinData();
                    temp.setId(single.getString(PinData.PARSE_ID));
                    try {
                        temp.setCreatedAt(format.parse(single.getString(PinData.PARSE_CREATED_AT)));

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    temp.setWidth(single.getInt(PinData.PARSE_WIDTH));
                    temp.setHeight(single.getInt(PinData.PARSE_HEIGHT));
                    temp.setColor(single.getString(PinData.PARSE_COLOR));
                    temp.setLikes(single.getInt(PinData.PARSE_LIKES));
                    temp.setLikedByUser(single.getBoolean(PinData.PARSE_LIKED_BY_USER));

                    //get user
                    nestedObj = single.getJSONObject(PinData.PARSE_USER);
                    userTemp = new User();
                    userTemp.setId(nestedObj.getString(User.PARSE_ID));
                    userTemp.setUsername(nestedObj.getString(User.PARSE_USERNAME));
                    userTemp.setName(nestedObj.getString(User.PARSE_NAME));

                    superNestedObj = nestedObj.getJSONObject(User.PARSE_PROFILE_IMAGE);
                    profileTemp = new ProfileImage();
                    profileTemp.setSmall(superNestedObj.getString(ProfileImage.PARSE_SMALL));
                    profileTemp.setMedium(superNestedObj.getString(ProfileImage.PARSE_MEDIUM));
                    profileTemp.setLarge(superNestedObj.getString(ProfileImage.PARSE_LARGE));
                    userTemp.setProfileImage(profileTemp);

                    superNestedObj = nestedObj.getJSONObject(User.PARSE_LINKS);
                    linkTemp = new Link();
                    linkTemp.setSelf(superNestedObj.getString(Link.PARSE_SELF));
                    linkTemp.setHtml(superNestedObj.getString(Link.PARSE_HTML));
                    linkTemp.setPhotos(superNestedObj.getString(Link.PARSE_PHOTOS));
                    linkTemp.setLikes(superNestedObj.getString(Link.PARSE_LIKES));
                    userTemp.setLinks(linkTemp);

                    // add user to pin data
                    temp.setUser(userTemp);

                    //get urls
                    nestedObj = single.getJSONObject(PinData.PARSE_URLS);
                    urlTemp = new Url();
                    urlTemp.setRaw(nestedObj.getString(Url.PARSE_RAW));
                    urlTemp.setFull(nestedObj.getString(Url.PARSE_FULL));
                    urlTemp.setRegular(nestedObj.getString(Url.PARSE_REGULAR));
                    urlTemp.setSmall(nestedObj.getString(Url.PARSE_SMALL));
                    urlTemp.setThumb(nestedObj.getString(Url.PARSE_THUMB));

                    // add url to pin data
                    temp.setUrls(urlTemp);

                    //get links
                    nestedObj = single.getJSONObject(PinData.PARSE_LINKS);
                    linkTemp = new Link();
                    linkTemp.setSelf(nestedObj.getString(Link.PARSE_SELF));
                    linkTemp.setHtml(nestedObj.getString(Link.PARSE_HTML));
                    linkTemp.setDownload(nestedObj.getString(Link.PARSE_DOWNLOAD));

                    // add url to pin data
                    temp.setLinks(linkTemp);

                    //get categories
                    nestedArrayObj = single.getJSONArray(PinData.PARSE_CATEGORIES);
                    catLisTemp = new ArrayList<Category>();
                    catSize = nestedArrayObj.length();
                    for (int j = 0; j < catSize; j++) {
                        categoryTemp = new Category();
                        categoryTemp.setTitle(((JSONObject)nestedArrayObj.get(j)).getString(Category.PARSE_TITLE));
                        catLisTemp.add(categoryTemp);
                    }

                    // add category to pin data
                    temp.setCategories(catLisTemp);
                    pinlist.add(temp);
                }

                //got the data. download the images.
                dataListener.onDataLoadComplete(pinlist);
                Log.d(TAG, "parse json done. " + pinlist.size());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public interface DataListener {
        void onDataLoadComplete(ArrayList<PinData> pinData);
    }
}
