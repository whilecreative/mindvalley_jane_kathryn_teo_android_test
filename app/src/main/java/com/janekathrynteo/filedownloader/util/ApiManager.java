package com.janekathrynteo.filedownloader.util;

import android.os.AsyncTask;
import android.util.Log;

import com.janekathrynteo.filedownloader.model.WebAPIData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Janek on 09/10/16.
 */
public class ApiManager extends AsyncTask<WebAPIData, Void, String> {

    private static final String TAG = "ApiManager";
    ApiListener apiListener;

    public ApiManager(ApiListener listener) {
        this.apiListener = listener;
    }

    protected String doInBackground(WebAPIData... params) {
        WebAPIData data = params[0];
        if (data.getLink() != null) {

            URL url;
            HttpURLConnection connection = null;

            try {
                //Create connection
                url = new URL(data.getLink());
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setConnectTimeout(5000); //5secs
                connection.setUseCaches(true);
                connection.setDoOutput(true);

                connection.connect();

                switch (connection.getResponseCode()) {
                    case 200:
                    case 201:
                        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();
                        return sb.toString();

                    default:
                        return "Error: " + connection.getResponseCode();
                }

            } catch (Exception e) {

                e.printStackTrace();
                Log.e(TAG, "Error: " + e.getMessage());

            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        } else {
            Log.e(TAG, "Link was null.");
        }

        return null;
    }

    @Override
    protected void onPostExecute(String data) {
        //process response data
        if (data != null) {
            apiListener.onRemoteCallComplete(data);
        }
    }

    public interface ApiListener {
        void onRemoteCallComplete(String responseData);
    }

}
