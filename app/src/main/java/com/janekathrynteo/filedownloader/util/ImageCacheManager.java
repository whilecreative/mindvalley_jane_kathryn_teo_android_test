package com.janekathrynteo.filedownloader.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.janekathrynteo.filedownloader.model.PinData;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Janek on 23/05/16.
 */
public class ImageCacheManager {

    public static final String TAG = "ImageCacheManager";
    private static ImageCacheManager instance;
    CacheManager cacheManager = new CacheManager();

    //Create Map (collection) to store image and image url in key value pair
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(
            new WeakHashMap<ImageView, String>());
    ExecutorService executorService;

    //handler to display images in UI thread
    Handler handler = new Handler();

    // default image show in list (Before online image download)
    final int stub_id = android.R.drawable.star_on;

    private ImageCacheManager() {
        executorService = Executors.newFixedThreadPool(5);
    }

    public static ImageCacheManager getInstance() {
        if (instance == null)
            instance = new ImageCacheManager();
        return instance;
    }

    public void DisplayImage(PinData data, ImageView imageView) {
        String url = data.getUrls().getSmall();
        Log.d(TAG, "url: " + url);

        //Store image and url in Map
        imageViews.put(imageView, url);

        //Check image is stored in MemoryCache Map or not (see MemoryCache.java)
        Bitmap bitmap = cacheManager.get(url);

        if (bitmap != null) {
            // if image is stored in MemoryCache Map then
            // Show image in listview row
            imageView.setImageBitmap(bitmap);
            Log.d(TAG, "cached image");
        } else {
            //queue Photo to download from url
            queuePhoto(url, imageView);

            //Before downloading image show default image
            imageView.setImageResource(stub_id);
            Log.d(TAG, "image not cached");

        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        // Store image and url in PhotoToLoad object
        PhotoToLoad p = new PhotoToLoad(url, imageView);

        // pass PhotoToLoad object to PhotosLoader runnable class
        // and submit PhotosLoader runnable to executers to run runnable
        // Submits a PhotosLoader runnable task for execution

        executorService.submit(new PhotosLoader(p));
    }

    //Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            try {
                //Check if image already downloaded
                if (imageViewReused(photoToLoad))
                    return;
                // download image from web url
                Bitmap bmp = getBitmap(photoToLoad.url);

                // set image data in Memory Cache
                cacheManager.put(photoToLoad.url, bmp);

                if (imageViewReused(photoToLoad))
                    return;

                // Get bitmap to display
                BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);

                // Causes the Runnable bd (BitmapDisplayer) to be added to the message queue.
                // The runnable will be run on the thread to which this handler is attached.
                // BitmapDisplayer run method will call
                handler.post(bd);

            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private Bitmap getBitmap(String link) {

        // Download image file from web
        try {

            InputStream is = null;
            if (link != null) {

                URL url;
                HttpURLConnection connection = null;

                try {
                    //temp
                    link = link.replaceFirst("https", "http");
                    //Create connection
                    url = new URL(link);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(5000); //5secs
                    connection.setRequestMethod("GET");
                    connection.setUseCaches(true);
                    connection.setDoOutput(false);

                    connection.connect();

                    Log.d(TAG, "downloading image");

                    if(connection.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST){
                        Log.e(TAG, "Error: Baaaad Request " + connection.getErrorStream());
                        return null;
                    }else{
                        int length = connection.getContentLength();
                        if (length <= 0) {
                            Log.e(TAG, "Filesize is 0. Error.");
                        }
                        is = connection.getInputStream();
                        return BitmapFactory.decodeStream(is);
                    }

                } catch (Exception e) {

                    e.printStackTrace();
                    Log.e(TAG, "Error: " + e.getMessage());

                } finally {
                    try {
                        if (connection != null) {
                            connection.disconnect();
                        }

                        if (is != null) {
                            is.close();
                        }

                    } catch (IOException ex) {
                        Log.e(TAG, ex.getMessage());
                    }
                }

            }

        } catch (Throwable ex) {
            ex.printStackTrace();
            if (ex instanceof OutOfMemoryError)
                cacheManager.clear();

        }

        return null;

    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {

        String tag = imageViews.get(photoToLoad.imageView);
        //Check url is already exist in imageViews MAP
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            // Show bitmap on UI
            if (bitmap != null)
                photoToLoad.imageView.setImageBitmap(bitmap);
            else
                photoToLoad.imageView.setImageResource(stub_id);
        }
    }

    public void clearCache() {
        //Clear cache directory downloaded images and stored data in maps
        cacheManager.clear();
    }

}