package com.janekathrynteo.filedownloader.viewmodel.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.janekathrynteo.filedownloader.R;
import com.janekathrynteo.filedownloader.model.PinData;
import com.janekathrynteo.filedownloader.util.ImageCacheManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Janek on 16/10/2016.
 */

public class PinAdapter extends BaseAdapter {

    private static final String TAG = "PinAdapter";
    private List<PinData> pinData;
    private static LayoutInflater inflater = null;
    public ImageCacheManager imageCacheManager;

    public PinAdapter(Activity act, ArrayList<PinData> pinData) {
        this.pinData = pinData;
        inflater = (LayoutInflater) act.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Create ImageLoader object to download and show image in list
        // Call ImageLoader constructor to initialize FileCache
        imageCacheManager = ImageCacheManager.getInstance();
    }

    public int getCount() {
        return pinData.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public ImageView image;

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {

            vi = inflater.inflate(R.layout.list_pin, null);

            holder = new ViewHolder();
            holder.image = (ImageView) vi.findViewById(R.id.pinPhoto);

            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        ImageView image = holder.image;

        //DisplayImage function from ImageLoader Class
        imageCacheManager.DisplayImage(pinData.get(position), image);
        Log.d(TAG, "adapter hit");
        return vi;
    }
}