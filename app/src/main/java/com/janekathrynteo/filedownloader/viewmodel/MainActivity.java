package com.janekathrynteo.filedownloader.viewmodel;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.janekathrynteo.filedownloader.R;
import com.janekathrynteo.filedownloader.model.PinData;
import com.janekathrynteo.filedownloader.model.WebAPIData;
import com.janekathrynteo.filedownloader.util.DataManager;
import com.janekathrynteo.filedownloader.viewmodel.adapter.PinAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements DataManager.DataListener, SwipeRefreshLayout.OnRefreshListener {

    private ListView pinList;
    private PinAdapter adapter;
    private DataManager dataManager;
    private WebAPIData data;
    private SwipeRefreshLayout swipeLayout;
    private TextView refreshText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pinList = (ListView) findViewById(R.id.pinListView);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeLayout.setOnRefreshListener(this);
        refreshText = (TextView) findViewById(R.id.pullToRefresh);
//
//        data = new WebAPIData("http://pastebin.com/raw/wgkJgazE", null);
//        dataManager = new DataManager();
//        dataManager.getPinData(data, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onDataLoadComplete(ArrayList<PinData> data){

        // Create custom adapter for listview
        adapter = new PinAdapter(this, data);

        //Set adapter to listview
        pinList.setAdapter(adapter);
        swipeLayout.setRefreshing(false);

    }

    @Override public void onRefresh() {
        refreshText.setVisibility(View.GONE);
        data = new WebAPIData("http://pastebin.com/raw/wgkJgazE", null);
        dataManager = new DataManager();
        dataManager.getPinData(data, this);
    }
}
