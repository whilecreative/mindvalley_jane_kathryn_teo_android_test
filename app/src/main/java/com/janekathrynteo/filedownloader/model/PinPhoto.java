package com.janekathrynteo.filedownloader.model;

import android.widget.ImageView;

/**
 * Created by Janek on 16/10/2016.
 */

public class PinPhoto {
    ImageView imageView;
    String url;
    String name;

    public PinPhoto(){}

    public PinPhoto(ImageView imageView, String url, String name){
        this.imageView = imageView;
        this.url = url;
        this.name = name;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
