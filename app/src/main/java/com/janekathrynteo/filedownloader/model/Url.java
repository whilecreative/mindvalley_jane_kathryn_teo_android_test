package com.janekathrynteo.filedownloader.model;

/**
 * Created by Janek on 09/10/2016.
 */

public class Url {
    public static final String PARSE_RAW = "raw";
    public static final String PARSE_FULL = "full";
    public static final String PARSE_REGULAR = "regular";
    public static final String PARSE_SMALL = "small";
    public static final String PARSE_THUMB = "thumb";

    private String raw;
    private String full;
    private String regular;
    private String small;
    private String thumb;

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
