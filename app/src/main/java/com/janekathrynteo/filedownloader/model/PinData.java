package com.janekathrynteo.filedownloader.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Janek on 09/10/2016.
 */

public class PinData {

    public static final String PARSE_ID = "id";
    public static final String PARSE_CREATED_AT = "created_at";
    public static final String PARSE_WIDTH = "width";
    public static final String PARSE_HEIGHT = "height";
    public static final String PARSE_COLOR = "color";
    public static final String PARSE_LIKES = "likes";
    public static final String PARSE_LIKED_BY_USER = "liked_by_user";
    public static final String PARSE_USER = "user";
    public static final String PARSE_CURRENT_USER_COLLECTIONS = "current_user_collections";
    public static final String PARSE_URLS= "urls";
    public static final String PARSE_CATEGORIES = "categories";
    public static final String PARSE_LINKS = "links";

    private String id;
    private Date createdAt;
    private int width;
    private int height;
    private String color;
    private int likes;
    private boolean likedByUser;
    private User user;
    private Url urls;
    private ArrayList<Category> categories;
    private Link links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean getLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Url getUrls() {
        return urls;
    }

    public void setUrls(Url urls) {
        this.urls = urls;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public Link getLinks() {
        return links;
    }

    public void setLinks(Link links) {
        this.links = links;
    }
}
