package com.janekathrynteo.filedownloader.model;

/**
 * Created by Janek on 09/10/2016.
 */

public class User {
    public static final String PARSE_ID = "id";
    public static final String PARSE_USERNAME = "username";
    public static final String PARSE_NAME = "name";
    public static final String PARSE_PROFILE_IMAGE = "profile_image";
    public static final String PARSE_LINKS = "links";

    private String id;
    private String username;
    private String name;
    private ProfileImage profileImage;
    private Link links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProfileImage getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    public Link getLinks() {
        return links;
    }

    public void setLinks(Link links) {
        this.links = links;
    }
}
