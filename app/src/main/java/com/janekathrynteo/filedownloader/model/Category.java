package com.janekathrynteo.filedownloader.model;

/**
 * Created by Janek on 09/10/2016.
 */

public class Category {

//    public static final String PARSE_ID = "id";
    public static final String PARSE_TITLE= "title";
//    public static final String PARSE_PHOTO_COUNT = "photo_count";
//    public static final String PARSE_LINKS = "links";


//    private String id;
    private String title;
//    private int photoCount;
//    private Link links;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
