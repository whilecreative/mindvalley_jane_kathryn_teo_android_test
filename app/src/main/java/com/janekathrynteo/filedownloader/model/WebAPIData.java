package com.janekathrynteo.filedownloader.model;

/**
 * Created by Janek on 07/07/16.
 * Use for post/get/response data for web api communication
 */
public class WebAPIData {
    private String link;
    private String postData;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public WebAPIData(String link, String postData){
        this.link = link;
        this.postData = postData;
    }
}
