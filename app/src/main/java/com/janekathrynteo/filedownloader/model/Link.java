package com.janekathrynteo.filedownloader.model;

/**
 * Created by Janek on 09/10/2016.
 */

public class Link {
    public static final String PARSE_SELF = "self";
    public static final String PARSE_HTML = "html";
    public static final String PARSE_DOWNLOAD = "download";
    public static final String PARSE_PHOTOS = "photos";
    public static final String PARSE_LIKES = "likes";

    private String self;
    private String html;
    private String download;
    private String photos;
    private String likes;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
