package com.janekathrynteo.filedownloader.model;

/**
 * Created by Janek on 09/10/2016.
 */

public class ProfileImage {
    public static final String PARSE_SMALL = "small";
    public static final String PARSE_MEDIUM= "medium";
    public static final String PARSE_LARGE = "large";

    private String small;
    private String medium;
    private String large;

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }
}
