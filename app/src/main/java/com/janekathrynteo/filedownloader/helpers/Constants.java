package com.janekathrynteo.filedownloader.helpers;

/**
 * Created by Janek on 06/07/16.
 */
public class Constants {
    public static int CACHE_SIZE = 20;
    public static String IMAGE_FOLDER = "/images/";
}
